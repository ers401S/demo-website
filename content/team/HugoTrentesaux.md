+++
title = "HugoTrentesaux"
description = "author of Albatros Theme"

[extra]
full_name = "Hugo Trentesaux"
avatar = "HugoTrentesaux.png"
website = "https://trentesaux.fr/"
email = "hugo@trentesaux.fr"

[taxonomies]
authors = ["HugoTrentesaux"]
+++

I'm the Author of this theme :)