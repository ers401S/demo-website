+++
title = "Albatros"
description = "short description"

[extra]
full_name = "Mister Albatros"
avatar = "albatros.png"
website = "https://example.com/"
phone = "+33 6 12 34 56 78"
email = "mail@domaine.tld"
xmpp = "pseudo@domaine.tld"

[taxonomies]
authors = ["Albatros"]
+++

Long description.